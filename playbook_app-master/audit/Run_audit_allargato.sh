#!/usr/bin/env bash
clear
ROSSO="\033[31m"
VERDE="\033[32m"
GIALLO="\033[33m"
VIOLA="\033[35m"
BIANCO="\033[37m"
BLU="\033[34;1m"
ACQUA="\033[36m"

echo "#############################################"
echo "## configurazione invio file audit jod-fcc ##"
echo "#############################################"
echo
echo "Log collector COLLAUDO 192.168.155.185 porta 1998"
echo "Log collector PRODUZIONE rete PDC 192.168.98.228 porta 1999"
echo "Log collector PRODUZIONE rete Postecom 169.254.169.241 porta 1999"
echo "-------------------------------------"
echo "SCEGLI L'AMBIENTE                    "
echo "-------------------------------------"
echo " 1 ) Collaudo                       "
echo " 2 ) Certifica                      "
echo " 3 ) Produzione                     "
echo "____________________________________"
read -p "indica l'ambiente 1,2 o 3 : " ENV
printf "\n\n"
scelata_ambinete() {
if [ $ENV -gt 3 ]
        then
           printf "\a $ROSSO - SCELTA ERRATA - $BIANCO\n"
           sleep 1
           exit 0
        else
         return $ENV
fi
}
AMBIENTE=""
if [ $ENV == 1 ]
  then
    AMBIENTE="Test"
    printf "Ambiente scelto : "
    printf "\a $VERDE - COLLAUDO - $BIANCO\n"

elif [ $ENV == 2 ]
  then
    AMBIENTE="Cert"
    printf "Ambiente scelto : "
    printf "\a $BLU - CERTIFICA - $BIANCO\n"
elif [ $ENV == 3 ]
  then
    AMBIENTE="Prod"
    printf "Ambiente scelto : "
    printf "\a $ROSSO - PRODUZIONE - $BIANCO\n"
else
   printf "devi sceglere una opzione tra 1 - 2 - 3\n"
   exit 0
fi

read -p "Host target: " TARGETHOSTS
#read -p "IP Log Collector: " IPCOLLECTOR
#read -p "Port Collector: " PORTCOLLECTOR

#TARGETHOSTS=audit_allargato-prod
IPCOLLECTOR=169.254.169.241
PORTCOLLECTOR=1999
PLAYBOOK_HOSTS=hosts
echo
echo "Ambiente ------------------>> " $AMBIENTE
echo "Hosts --------------------->> " $TARGETHOSTS
echo "Ip collector -------------->> " $IPCOLLECTOR
echo "Port collector ------------>> " $PORTCOLLECTOR
echo "variable_host ------------->> " $PLAYBOOK_HOSTS
echo
ansible-playbook -k -K  -u mcuccaro audit_allargato.yml -i hosts --extra-var "target_hosts=$TARGETHOSTS ipcollector=$IPCOLLECTOR port_collector=$PORTCOLLECTOR ambiente=$AMBIENTE"
