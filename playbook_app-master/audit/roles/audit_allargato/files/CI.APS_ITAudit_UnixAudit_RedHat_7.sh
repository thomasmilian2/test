#!/bin/sh
#
# SCRIPT: CI.APS_ITAudit_UnixAudit_RedHat_7.sh
# AUTORE: Claudio Bettozzi
# DATA: 22 Ottobre 2018
# REV: 3.0.P (opzioni valide sono A, B, D, T and P)
# (A Alpha, B Beta, D Dev, T Test e P Produzione)
#
# PLATFORM: Red Hat Enterprise Linux 7 + (Apache se presente, Jboss se presente, Tomcat se presente)
#
# SCOPO: Questo script ha lo scopo raccogliere le informazioni di sistema
#				 e le configurazioni di sicurezza impostate su una di piattaforma Unix
#				 per effetture un Audit di Sicurezza 
#
#
# REVISIONI:
# DATA:
# AUTORE:
# MODIFICHE: 
#
#
#set -n # Eliminare il commento per verificare la sintassi 
#        # dello script senza eseguirlo.
#
######################################################################
###################### FILE E VARIABLI ###############################
######################################################################
# Crea la directory "audit" dove memorizzare i risultati dell'esecuzione
# nel caso non sia stata gia creata

if  [ ! -e "audit" ]
then
	echo "--> Creazione directory audit per memorizzazione risultati"
	mkdir audit
fi

REPORT_FILE="audit/audit-report.RedHat.$(hostname).$(date +%d%m%Y)_$(date +%H%M%S).txt"
REPORT_FILE_WITH_ERROR="audit/audit-report.RedHat_with_error.$(hostname).$(date +%d%m%Y)_$(date +%H%M%S).txt"
(
#Variabili utilizzate durante la verifica della presenza di informazioni sensibili nel server in analisi
ANALISI_CONT_FILE="audit/AnalisiContenutoFile.$(hostname)/StringheRilevate.$(hostname).$(date +%d%m%Y)_$(date +%H%M%S).txt"
ANALISI_CONT_FILE_ELENCO="audit/AnalisiContenutoFile.$(hostname)/ElencoFileAnalizzati.$(hostname).$(date +%d%m%Y)_$(date +%H%M%S).txt"


# Stabilisce la tipologia del sistema operativo UNIX installato
# e valorizza determinate variabili in accordo

os=`uname -s`
hw=`uname -m`
rootdir="/"

# 
export LANG=C

echo='/bin/echo -e'
ps='ps -ef'
homedir="/home"

# VARIABILI PER SICUREZZA APACHE
COMMAND="`ps -ef | grep "httpd" | grep -v "grep"`"
APACHE_USER="`echo "$COMMAND" | awk '{print $1}'`"
for arg in $(find / -path "*share*" -prune -o -path "*Trash*" -prune -o -path "/tmp/*" -prune -o -path "/mnt/*" -prune -o -path "/log/*" -prune -o -path "*/lib*" -prune -o -path "*/proc/*" -prune -o -path "*/media/*" -prune -o -path "*/home/*" -prune -o \( -name "httpd" -o -name "bin" -o -name "httpd.conf" -o -name "cgi-bin" -o -name "ssl.conf" \) -print)
do
	if [[ $arg =~ "httpd" ]]
	then
		if [[ $arg =~ "bin" ]]
			then
		  	APACHE_HTTPD_PATH=$arg
				APACHE_HOME_BIN=`dirname "$arg"` #Trasforma il path comprensivo del nome del file cercato (es: httpd) in solo path
				echo $APACHE_HTTPD_PATH
				echo $APACHE_HOME_BIN
			fi
  fi
	if [[ $arg =~ "httpd.conf" ]]
	then
				APACHE_HOME=`dirname "$arg"` #Trasforma il path comprensivo del nome del file cercato (es: httpd.conf) in solo path
				#echo $arg
				echo $APACHE_HOME
  fi
	if [[ $arg =~ "cgi-bin" ]]
	then
				APACHE_WEB=`dirname "$arg"` #Trasforma il path comprensivo del nome del file cercato (es: cgi-bin) in solo path
				#echo $arg
				echo $APACHE_WEB
  fi  
	if [[ $arg =~ "ssl.conf" ]]
	then
				APACHE_WEB=`dirname "$arg"` #Trasforma il path comprensivo del nome del file cercato (es: ssl.conf) in solo path
				#echo $arg
				echo $SSL_HOME
  fi   
done
######################################################################
###################### FUNZIONI ######################################
######################################################################

# Funzione per scrivere il risultato di un "echo" direttamente sul file 
# senza dover reindirizzare ciascuna riga.

write () {
	$echo "$*"
	$echo "$*" >> $REPORT_FILE
}

WriteElencoFileAnalizzati () {
	$echo "$*"
	$echo "$*" >> $ANALISI_CONT_FILE_ELENCO
}

WriteStringheRilevate () {
	echo "+++ PRESENZA DI INFORMAZIONI SENSIBILI NEL FILE "$*"  +++"
	$echo "+++++++++++++ PRESENZA DI INFORMAZIONI SENSIBILI NEL FILE "$*" ++++++++++ +++" >> $ANALISI_CONT_FILE 
	$echo "+++++++++++++ Permessi del file "$*" ++++++++++ +++" >> $ANALISI_CONT_FILE 
	ls -l "$*" >> $ANALISI_CONT_FILE
	
	$echo "" >> $ANALISI_CONT_FILE	
	$echo "+++++++++++++ Stringhe rilevate nel file "$*" ++++++++++ +++" >> $ANALISI_CONT_FILE 
	$echo "////////////////////////////////////////////////////////////////////////////////////////////////////////" >> $ANALISI_CONT_FILE 
	grep -E -I -i "username=|username:|user:|user=|password=|password:|pwd|pass=|pass:|passwd|psw|admin=|admin:|adm=|adm:|administrator|root:|root=|CVV2|4023600|dbadm|'\b[a-z0-9]{1,}@*\.(it|eu|com|net|org|tv)\b'" "$*" >> $ANALISI_CONT_FILE
	$echo "////////////////////////////////////////////////////////////////////////////////////////////////////////" >> $ANALISI_CONT_FILE
}

function Tomcat_Restrict_Admin_Manager_Tmp
{
    COMMENT="no"
        
    if [ "`find $TOMCAT_HOME -type f -name "$1" 2>/devull | wc -l`" -gt 0 ]
    then
        find $TOMCAT_HOME -type f -name "$1" 2>/devull | while read line
        do
        		cat $line | while read line2
            do
                if [[ "$line2" =~ "<!--" ]]
                then
                    COMMENT="yes"
                        
                fi
                
                if [ "$COMMENT" == "no" ]
                then
                    if [[ "$line2" =~ "<Valve.*allow=(.*)/>" ]]
                    then
                      write "+++ Info: -> ? stato ristretto l'accesso a $line=allow${BASH_REMATCH[1]}   +++"					
                      #echo "OK: -> ? stato ristretto l'accesso a $line=allow${BASH_REMATCH[1]}"
                      ristretto="OK"
                    else
                    	ristretto="KO"
                    fi
                fi
                
                if [[ "$line2" =~ "-->" ]]
                then
                    COMMENT="no"
                fi       
            done
            if [ "$ristretto" = "KO" ] 
        		then
        			write "+++ Attenzione: -> Non ? stato ristretto l'accesso a "$line"  +++"
        			#echo "Attenzione: -> Non ? stato ristretto l'accesso a "$line""
        		fi
        done

    fi    
}

function Tomcat_Extract_Conf
{
    CONTROL="no"
        
    
    if [ ! -f "$TOMCAT_HOME/conf/$1" ]
    then
    		write "+++ Errore Critico: -> Il file $TOMCAT_HOME/conf/$1 non esiste!!!   +++"					
    else
        cat $TOMCAT_HOME/conf/$1 | grep -E -v "^$" | grep -E -v "$2 .* $3" | while read line
        do
            if [[ "$line" =~ "$2" ]]
            then
                CONTROL="yes"
            fi
            
            if [ "$CONTROL" == "no" ]
            then
                echo "$line" >> $4
            fi
            
            if [[ "$line" =~ "$3" ]]
            then
                CONTROL="no"
            fi
        done
    fi   
}

######################################################################
###################### MAIN ##########################################
######################################################################

# Verifica se i privilegi di esecuzione sono di "root" altrimenti
# interrompe l'esecuzione

if [ $UID != 0 ]; then
    echo "Errore: effettuare SU a root per poter eseguire lo script."
    echo "Aborting..."
    exit 1
fi

# Intestazione del report

write "----------------------------------------------------_"
write "Controllo Interno"
write "Audit Processi Di Supporto - Audit Processi IT"
write "UNIX System Audit Script - PIATTAFORMA Red Hat Enterprise Linux 7"
write "Audit Report per il sistema $(hostname), $(date +%D)"
write "-----------------------------------------------------\n"


if [ "$APACHE_HOME" != "" ]
then
		# Crea la directory "audit/apache.$(hostname)" dove memorizzare i file di configurazione di apache
		# nel caso non sia stata gi? creata
		if  [ ! -e audit/apache.$(hostname) ]
			then
			mkdir audit/apache.$(hostname)
		fi
fi
write "Data Analisi:<$(date +%d%m%Y)_$(date +%H%M%S)>"
write "Inizio esecuzione script: $(date +%H%M%S) "

write "***************************************************************"    
write "INFORMAZIONI DI SISTEMA"
write "***************************************************************"    

write "+++NOME DEL SISTEMA OPERATIVO+++"
write "RedHat_7"
write "+++HOSTNAME+++"
uname -n 	>> $REPORT_FILE
write "+++CHIPSET+++"
uname -p  >> $REPORT_FILE    
write "+++VERSIONE DEL SISTEMA OPERATIVO+++"
uname -r >> $REPORT_FILE
write "+++MODELLO MACCHINA+++"
uname -m 	>> $REPORT_FILE
write "+++ INFORMAZIONI HARD-DISK +++"
 df -k >> $REPORT_FILE
                            
write "+++CONFIGURAZIONE NIC +++"
 ifconfig -a >> $REPORT_FILE
 
write "\n***************************************************************"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<0> +++"
write "+++ idCategoriaVerifica:<0> +++"
write "+++ idVerifica:<0.0.1> +++"
write "+++ RisultatoComando1 +++"
cat /etc/fstab > audit/etc_fstab.$(hostname).txt
write "+++ RisultatoComando2 +++"
cat /etc/yum.conf > audit/etc_yum.conf.$(hostname).txt
write "+++ RisultatoComando3 +++"
cat /etc/passwd > audit/etc_passwd.$(hostname).txt
write "+++ RisultatoComando4 +++"
cat /etc/shadow > audit/etc_shadow.$(hostname).txt
write "+++ RisultatoComando5 +++"
cat /etc/gshadow > audit/etc_gshadow.$(hostname).txt
write "+++ RisultatoComando6 +++"
cat /etc/group > audit/etc_group.$(hostname).txt
write "+++ RisultatoComando7 +++"
cat /etc/bashrc > audit/etc_bashrc.$(hostname).txt
write "+++ RisultatoComando8 +++"
cat /etc/profile > audit/etc_profile.$(hostname).txt
write "+++ RisultatoComando9 +++"
cat /etc/grub.conf > audit/etc_grub.conf.$(hostname).txt
write "+++ RisultatoComando10 +++"
cat /etc/sysconfig/init > audit/etc_sysconfig_init.$(hostname).txt
write "+++ RisultatoComando11 +++"
cat /etc/security/limits.conf > audit/etc_security_limits.conf.$(hostname).txt
write "+++ RisultatoComando12 +++"
cat /etc/sysctl.conf > audit/etc_sysctl.conf.$(hostname).txt
write "+++ RisultatoComando13 +++"
cat /etc/sysconfig/prelink > audit/etc_sysconfig_prelink.$(hostname).txt
write "+++ RisultatoComando14 +++"
cat /etc/sysconfig/network > audit/etc_sysconfig_network.$(hostname).txt
write "+++ RisultatoComando15 +++"
cat /etc/modprobe.d/ipv6.conf > audit/etc_modprobe.d_ipv6.conf.$(hostname).txt
write "+++ RisultatoComando16 +++"
cat /etc/hosts.allow > audit/etc_hosts.allow.$(hostname).txt
write "+++ RisultatoComando17 +++"
cat /etc/hosts.deny > audit/etc_hosts.deny.$(hostname).txt
write "+++ RisultatoComando18 +++"
cat /etc/login.defs > audit/etc_login.defs.$(hostname).txt
write "+++ RisultatoComando19 +++"
cat /etc/pam.d/system-auth > audit/etc_pam.d_system-auth.$(hostname).txt
write "+++ RisultatoComando20 +++"
cat /etc/ssh/sshd_config > audit/etc_ssh_sshd_config.$(hostname).txt
write "+++ RisultatoComando21 +++"
cat /etc/pam.d/su > audit/etc_pam.d_su.$(hostname).txt
write "+++ RisultatoComando22 +++"
cat /etc/motd > audit/etc_motd.$(hostname).txt
write "+++ RisultatoComando23 +++"
cat /etc/issue > audit/etc_issue.$(hostname).txt
write "+++ RisultatoComando24 +++"
cat /etc/issue.net > audit/etc_issue.net.$(hostname).txt
write "+++ RisultatoComando25 +++"
cat /etc/pam.d/password-auth > audit/etc_pam.d_password-auth.$(hostname).txt
write "+++ RisultatoComando26 +++"
cat /etc/rsyslog.conf > audit/etc_rsyslog.conf.$(hostname).txt
write "+++ RisultatoComando27 +++"
cat /etc/audit/audit.rules > audit/etc_audit_audit.rules.$(hostname).txt
write "+++ RisultatoComando28 +++"
cat /etc/audit/auditd.conf > audit/etc_audit_auditd.conf.$(hostname).txt
write "+++ RisultatoComando29 +++"
chkconfig --list >> audit/ServiziAttivi.$(hostname).txt
write "+++ RisultatoComando30 +++"
systemctl >> audit/ServiziAttivi.$(hostname).txt
write "+++ RisultatoComando31 +++"
ps -ef >> audit/ProcessiAttivi.$(hostname).txt
write "+++ RisultatoComando32 +++"
env > audit/VariabiliAmbiente.$(hostname).txt
write "+++ RisultatoComando33 +++"
cat /etc/ntp.conf > audit/etc_ntp.conf.$(hostname).txt
write "+++ RisultatoComando34 +++"
cat /etc/sssd/sssd.conf > audit/sssd_sssd.conf.$(hostname).txt
write "+++ RisultatoComando35 +++"
cat /etc/security/access.netgroup.conf > audit/security_access.netgroup.conf.$(hostname).txt
write "+++ RisultatoComando36 +++"
cat /boot/grub2/grub.cfg > audit/boot_grub2_grub.cfg.txt
write "+++ RisultatoComando37 +++"
cat /usr/lib/systemd/system/rescue.service > audit/usr_lib_systemd_system_rescue.service.txt
write "+++ RisultatoComando38 +++"
cat /usr/lib/systemd/system/emergency.service > audit/usr_lib_systemd_system_emergency.service.txt
write "+++ RisultatoComando39 +++"
cat /etc/selinux/config > audit/etc_selinux_config.txt
write "+++ RisultatoComando40 +++"
cat /etc/dconf/profile/gdm > audit/etc_dconf_profile_gdm.txt
write "+++ RisultatoComando41 +++"
cat /etc/chrony.conf > audit/etc_chrony.conf.txt
write "+++ RisultatoComando42 +++"
cat /etc/sysconfig/chronyd > audit/etc_sysconfig_chronyd.txt
write "+++ RisultatoComando43 +++"
cat /etc/syslog-ng/syslog-ng.conf > audit/etc_syslog-ng_syslog-ng.conf.txt
write "+++ RisultatoComando44 +++"
cat /etc/securetty > audit/etc_securetty.txt
write "+++ RisultatoComando45 +++"
cat /etc/crontab > audit/etc_crontab.txt
write "+++ RisultatoComando46 +++"
ls /etc/cron.hourly|while read line;do cat /etc/cron.hourly/$line > audit/etc_cron.hourly_$line.txt;done
write "+++ RisultatoComando47 +++"
ls /etc/cron.daily|while read line;do cat /etc/cron.daily/$line > audit/etc_cron.daily_$line.txt;done
write "+++ RisultatoComando48 +++"
ls /etc/cron.weekly|while read line;do cat /etc/cron.weekly/$line > audit/etc_cron.weekly_$line.txt;done
write "+++ RisultatoComando49 +++"
ls /etc/cron.monthly|while read line;do cat /etc/cron.monthly/$line > audit/etc_cron.monthly_$line.txt;done
write "+++ RisultatoComando50 +++"
cat /etc/cron.d > audit/etc_cron.d.txt
write "+++ RisultatoComando51 +++"
cat /etc/cron.deny > audit/etc_cron.deny.txt
write "+++ RisultatoComando52 +++"
cat /etc/at.deny > audit/etc_at.deny.txt
write "+++ RisultatoComando53 +++"
sysctl -a > audit/sysctl.$(hostname).txt
write "+++ RisultatoComando54 +++"
netstat -tulnp | tr -s ' ' '@' > audit/Servizi_porte_attive.$(hostname).txt
write "+++ RisultatoComando55 +++"
last -a -n 5000 > audit/Last_user_logon.$(hostname).txt
write "+++ RisultatoComando56 +++"
lastlog > audit/Last_user_logon_lastlog.$(hostname).txt
write "+++ RisultatoComando57 +++"
cat /etc/sudoers > audit/etc_sudoers.$(hostname).txt
write "+++ RisultatoComando58 +++"
cat /var/log/secure > audit/var_log_secure.$(hostname).txt
write "+++ RisultatoComando59 +++"
cat /var/log/audit/audit.log > audit/var_log_audit_audit_log.$(hostname).txt
write "+++ RisultatoComando60 +++"
cat /etc/services > audit/etc_services.$(hostname).txt
write "+++ RisultatoComando61 +++"
rpm -qa > audit/Lista_pacchetti_installati.$(hostname).txt
write "+++ RisultatoComando62 +++"
if [ -e /opt/oracle_12.1.3/ ] ; then tar -czf audit/Config_AS_Weblogic.$(hostname).tar.gz /opt/oracle_12.1.3; fi
write "+++ RisultatoComando63 +++"
if [ -e /opt/apache/ ] ; then tar -czf audit/Config_WS_Apache.$(hostname).tar.gz /opt/apache; fi
write "+++ RisultatoComando64 +++"
if [ -e /etc/httpd/ ] ; then tar -czf audit/Config_WS_Apache.$(hostname).tar.gz /etc/httpd; fi
write "+++ RisultatoComando65 +++"
if [ -e /etc/httpd/ ] ; then tar -czf audit/Config_WS_Apache.$(hostname).tar.gz /etc/httpd; fi
write "+++ RisultatoComando66 +++"
if [ -e /opt/tomcat-auth/ ] ; then tar -czf audit/Config_AS_Tomcat.$(hostname).tar.gz /opt/tomcat-auth; fi
write "+++ RisultatoComando67 +++"
if [ -e /opt/tomcat-mobilebp/ ] ; then tar -czf audit/Config_AS_Tomcat.$(hostname).tar.gz /opt/tomcat-mobilebp; fi
write "+++ RisultatoComando68 +++"
if [ -e /opt/tomcat/ ] ; then tar -czf audit/Config_AS_Tomcat.$(hostname).tar.gz /opt/tomcat; fi
write "+++ RisultatoComando69 +++"
if [ -e /opt/apache-tomcat-8.0.32/ ] ; then tar -czf audit/Config_AS_Tomcat.$(hostname).tar.gz /opt/apache-tomcat-8.0.32; fi
write "+++ RisultatoComando70 +++"
if [ -e /opt/apache-tomcat-8.5.30/ ] ; then tar -czf audit/Config_AS_Tomcat.$(hostname).tar.gz /opt/apache-tomcat-8.5.30; fi
write "+++ RisultatoComando71 +++"
if [ -e /opt/apache-tomcat-8.5.33/ ] ; then tar -czf audit/Config_AS_Tomcat.$(hostname).tar.gz /opt/apache-tomcat-8.5.33; fi
write "+++ RisultatoComando72 +++"
if [ -e /opt/apache-tomcat/ ] ; then tar -czf audit/Config_AS_Tomcat.$(hostname).tar.gz /opt/apache-tomcat; fi
write "+++ RisultatoComando73 +++"
if [ -e /opt/apache-tomcat-7.0.42/ ] ; then tar -czf audit/Config_AS_Tomcat.$(hostname).tar.gz /opt/apache-tomcat-7.0.42; fi
write "+++ RisultatoComando74 +++"
if [ -e /etc/nginx/ ] ; then tar -czf audit/Config_WS_Nginx.$(hostname).tar.gz /etc/nginx; fi
write "+++ RisultatoComando75 +++"
if [ -e /opt/SUNWdsee/ ] ; then tar -czf audit/Config_DS_OpenLDAP_IAM.$(hostname).tar.gz /opt/SUNWdsee; fi
write "+++ RisultatoComando76 +++"
if [ -e /data/ldap_instances/ ] ; then tar -czf audit/Config_DS_LDAP_OUD.$(hostname).tar.gz /data/ldap_instances; fi
write "+++ RisultatoComando77 +++"
if [ -e /etc/redis_Redis_APP_Cluster/ ] ; then tar -czf audit/Config_AS_Redis.$(hostname).tar.gz /etc/redis_Redis_APP_Cluster; fi
write "+++ RisultatoComando78 +++"
if [ -e /opt/kafka_2.11-1.0.0/ ] ; then tar -czf audit/Config_AS_PFM_zookeeper-kafka.$(hostname).tar.gz /opt/kafka_2.11-1.0.0; fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<0> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<0.1.1> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
for arg in $APACHE_HOME
do
cp -r $arg audit/apache.$(hostname)
cp -r $arg/../modules audit/apache.$(hostname)
cp -r /etc/logrotate.conf audit/apache.$(hostname)
$APACHE_HTTPD_PATH -M >> audit/apache.$(hostname)/Apache_Moduli_Caricati.$(hostname).txt
done
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<1> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<1.1.1> +++"
write "+++ RisultatoComando1 +++"
 # "ANALISI MANUALE" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<1> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<1.2.1> +++"
write "+++ RisultatoComando1 +++"
 grep /tmp /etc/fstab >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
mount | grep /tmp >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep /var /etc/fstab >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
grep -e "^/tmp" /etc/fstab | grep /var/tmp >> $REPORT_FILE
write "+++ RisultatoComando5 +++"
grep /var/log /etc/fstab >> $REPORT_FILE
write "+++ RisultatoComando6 +++"
grep /var/log/audit /etc/fstab >> $REPORT_FILE
write "+++ RisultatoComando7 +++"
grep /home /etc/fstab >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<1> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<1.3.1> +++"
write "+++ RisultatoComando1 +++"
 grep ^gpgcheck /etc/yum.conf >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep ^gpgcheck /etc/yum.repos.d/* >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
yum check-update |tee  audit/H.IN_list-updates.$(hostname).txt >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
cat /etc/redhat-release >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<1> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 $APACHE_HTTPD_PATH -v >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"

if [ "$APACHE_HOME" != "" ]
then
 yum check-update $APACHE_HTTPD_PATH >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<1> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^ServerRoot" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"

if [ "$APACHE_HOME" != "" ]
then
 grep "^DocumentRoot" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<10> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_security.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<11> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_rewrite.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 for arg4 in $(find / -path '*Trash*' -prune -o -path '/tmp/*' -prune -o -path '*lib*' -prune -o -name ".htaccess" -print)
  do
  grep "RewriteEngine" $arg4 >> $REPORT_FILE
  done
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<12> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^LogLevel" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^ErrorLog" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^CustomLog" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<2> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^Listen" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<3> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^User" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"

if [ "$APACHE_HOME" != "" ]
then
 grep "^Group" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"

if [ "$APACHE_HOME" != "" ]
then
 echo $APACHE_USER >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 if [ "$(ls -A $APACHE_WEB/cgi-bin)" ]; then
     echo "La directory $APACHE_WEB/cgi-bin non_vuota" >> $REPORT_FILE
 else
    echo "La directory $APACHE_WEB/cgi-bin vuota" >> $REPORT_FILE
 fi
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 if [ "$(ls -A $APACHE_WEB/../htdocs)" ]; then
     echo "La directory $APACHE_WEB/htdocs non_vuota" >> $REPORT_FILE
 else
    echo "La directory $APACHE_WEB/htdocs vuota" >> $REPORT_FILE
 fi
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"
if [ "$APACHE_HOME" != "" ]
then
 if [ "$(ls -A $APACHE_WEB/manual)" ]; then
     echo "La directory $APACHE_WEB/manual non_vuota" >> $REPORT_FILE
 else
    echo "La directory $APACHE_WEB/manual vuota" >> $REPORT_FILE
 fi
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<5> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^ServerTokens" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^ServerSignature" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 ls -l $APACHE_HOME_BIN | awk '{print $1}' | grep -v "total" >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 ls -l $APACHE_HOME | awk '{print $1}' | grep -v "total" >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"
if [ "$APACHE_HOME" != "" ]
then
 ls -l $APACHE_WEB | awk '{print $1}' | grep -v "total" >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<7> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^LimitRequestBody" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^LimitRequestFields" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^LimitRequestFieldSize" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando4 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "LimitRequestLine" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<8> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^Timeout" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^KeepAlive " $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "^KeepAliveTimeout" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando4 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "AcceptFilter" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando5 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "AcceptFilter" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<10> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<9> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_authn_dbm.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_authn_dbd.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_authz_dbm.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando4 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_dbd.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando5 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_dumpio.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando6 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_substitute.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando7 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_ident.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando8 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_version.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando9 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_lua.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando10 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_cgi.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando11 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_charset_lite.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando12 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando13 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_connect.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando14 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_express.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando15 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_fcgi.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando16 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_ftp.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando17 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_fdpass.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando18 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_http.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando19 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_html.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando20 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_heartbeat.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando21 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_dialup.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando22 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_scgi.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando23 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_ajp.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando24 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_proxy_balancer.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando25 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_dav.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando26 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_status.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando27 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_autoindex.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando28 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_info.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando29 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_dav_fs.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando30 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_imagemap.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando31 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_speling.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando32 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_userdir.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando33 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_dav_lock.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando34 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_cache.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando35 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_disk_cache.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando36 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_cgid.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando37 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_echo.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando38 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_example.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando39 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_mem_cache.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando40 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_suexec.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando41 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_actions.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando42 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_lbmethod_buybusyness.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando43 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_lbmethod_byrequest.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando44 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_lbmethod_bytraffic.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando45 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_lbmethod_heartbeat.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando46 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_slotmem_plain.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando47 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_slotmem_shm.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando48 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_socache_dbm.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando49 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_socache_memcache.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando50 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_socache_dc.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando51 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_socache_shmcb.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando52 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_watchdog.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando53 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "mod_xml2enc.so" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<11> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<1> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "Order allow,deny" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "Allow from all" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<11> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<2> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 cat $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<11> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "#AddHandler cgi-script .cgi" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "#AddType text/html .shtml" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "#AddHandler server-parsed .shtml" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<11> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<4> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 cat $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando2 +++"
if [ "$APACHE_HOME" != "" ]
then
 cat $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "+++ RisultatoComando3 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "TraceEnable" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<11> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<5> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 grep "c(?:o(?:nf(?:ig)?|m)|s(?:proj|r)?|dx|er|fg|md)|p(?:rinter|ass|db|ol|wd)|v(?:b(?:proj|s)?|sdisco)|a(?:s ?:ax?|cx)|xd)|d(?:bf?|at|ll|os)|i(?:d[acq]|n[ci])|ba(?:[kt]| ckup)|res(?:ources|x)|s(?:h?tm|ql|ys)|l(?:icx|nk|og)|\w{,5}~|webinfo|ht[rw]|xs[dx]|key|mdb|old)" $APACHE_HOME/httpd.conf >> $REPORT_FILE
else
  echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<12> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<1> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
 rpm -qa | grep -i openssl >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<12> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<2> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
  grep "SSLCipherSuite" $SSL_HOME/ssl.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<12> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
  grep "SSLProtocol" $SSL_HOME/ssl.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<12> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<4> +++"
write "+++ RisultatoComando1 +++"
if [ "$APACHE_HOME" != "" ]
then
  grep "SSLInsecureRenegotiation" $SSL_HOME/ssl.conf >> $REPORT_FILE
else
 echo "Verifica Non Applicabile" >> $REPORT_FILE
fi 
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<2.1.1> +++"
write "+++ RisultatoComando1 +++"
grep dccp /etc/modprobe.d/*.conf >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
lsmod | grep dccp >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep sctp /etc/modprobe.d/*.conf >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
lsmod | grep -w sctp >> $REPORT_FILE
write "+++ RisultatoComando5 +++"
grep rds /etc/modprobe.d/*.conf >> $REPORT_FILE
write "+++ RisultatoComando6 +++"
lsmod | grep -w rds >> $REPORT_FILE
write "+++ RisultatoComando7 +++"
grep tipc /etc/modprobe.d/*.conf >> $REPORT_FILE
write "+++ RisultatoComando8 +++"
lsmod | grep -w tipc >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<2.2.1> +++"
write "+++ RisultatoComando1 +++"
 grep /tmp /etc/fstab | grep nodev >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
mount | grep /tmp | grep nodev >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep /tmp /etc/fstab | grep nosuid >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
mount | grep /tmp | grep nosuid >> $REPORT_FILE
write "+++ RisultatoComando5 +++"
grep /tmp /etc/fstab | grep noexec >> $REPORT_FILE
write "+++ RisultatoComando6 +++"
mount | grep /tmp | grep noexec >> $REPORT_FILE
write "+++ RisultatoComando7 +++"
mount | grep /tmp | grep relatime >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<2.2.2> +++"
write "+++ RisultatoComando1 +++"
 grep /home /etc/fstab >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
mount | grep /home | grep nodev >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<2.2.3> +++"
write "+++ RisultatoComando1 +++"
# "ANALISI MANUALE" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<2.2.4> +++"
write "+++ RisultatoComando1 +++"
mount | grep /dev/shm | grep nodev >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
mount | grep /dev/shm | grep nosuid >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
mount | grep /dev/shm | grep noexec >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.1> +++"
write "+++ RisultatoComando1 +++"
stat /etc/passwd >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.10> +++"
write "+++ RisultatoComando1 +++"
df --local -P | awk '{if (NR!=1) print $6}' | xargs -I '{}' find '{}' -xdev -type f -perm -0002 >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.11> +++"
write "+++ RisultatoComando1 +++"
 find / -path /proc -prune -o -name nas -prune -name nas_cdg -prune -perm -4000 -o -perm -2000 -print >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.12> +++"
write "+++ RisultatoComando1 +++"
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.13> +++"
write "+++ RisultatoComando1 +++"
grep "^umask" /etc/bashrc >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep "^umask" /etc/profile >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.14> +++"
write "+++ RisultatoComando1 +++"
stat /etc/ >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
stat /sbin/ >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.15> +++"
write "+++ RisultatoComando1 +++"
find /var/log/ -type f -ls | awk '{print $3}' >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.2> +++"
write "+++ RisultatoComando1 +++"
stat /etc/passwd- >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.3> +++"
write "+++ RisultatoComando1 +++"
stat /etc/shadow >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.4> +++"
write "+++ RisultatoComando1 +++"
stat /etc/shadow- >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.5> +++"
write "+++ RisultatoComando1 +++"
stat /etc/gshadow >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.6> +++"
write "+++ RisultatoComando1 +++"
stat /etc/gshadow- >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.7> +++"
write "+++ RisultatoComando1 +++"
stat /etc/group >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.8> +++"
write "+++ RisultatoComando1 +++"
stat /etc/group- >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<2.3.9> +++"
write "+++ RisultatoComando1 +++"
 find / -name nas -prune -name nas_cdg -prune -fstype nfs -o -fstype cachefs -o -fstype autofs -o -fstype ctfs -o -fstype mntfs -o -fstype objfs -o -fstype proc -prune -o -nouser -o -nogroup -ls >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<4> +++"
write "+++ idVerifica:<2.4.1> +++"
write "+++ RisultatoComando1 +++"
stat /boot/grub2/grub.cfg >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<4> +++"
write "+++ idVerifica:<2.4.2> +++"
write "+++ RisultatoComando1 +++"
grep /sbin/sulogin /usr/lib/systemd/system/rescue.service >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep /sbin/sulogin /usr/lib/systemd/system/emergency.service >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<4> +++"
write "+++ idVerifica:<2.4.3> +++"
write "+++ RisultatoComando1 +++"
 grep "^PROMPT=" /etc/sysconfig/init >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<5> +++"
write "+++ idVerifica:<2.5.1> +++"
write "+++ RisultatoComando1 +++"
 grep "hard core" /etc/security/limits.conf >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
sysctl fs.suid_dumpable >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<5> +++"
write "+++ idVerifica:<2.5.2> +++"
write "+++ RisultatoComando1 +++"
 sysctl kernel.exec-shield >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<5> +++"
write "+++ idVerifica:<2.5.3> +++"
write "+++ RisultatoComando1 +++"
 sysctl kernel.randomize_va_space >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<2> +++"
write "+++ idCategoriaVerifica:<6> +++"
write "+++ idVerifica:<2.6.1> +++"
write "+++ RisultatoComando1 +++"
 # "ANALISI MANUALE" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.1> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.ip_forward >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.10> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.tcp_syncookies >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.2> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.conf.all.send_redirects >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
/sbin/sysctl net.ipv4.conf.default.send_redirects >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.3> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.conf.all.accept_redirects >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
/sbin/sysctl net.ipv4.conf.default.accept_redirects >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.4> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.conf.all.secure_redirects >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
/sbin/sysctl net.ipv4.conf.default.secure_redirects >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.5> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.conf.all.accept_source_route >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
/sbin/sysctl net.ipv4.conf.default.accept_source_route >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.6> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.conf.all.log_martians >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
/sbin/sysctl net.ipv4.conf.default.log_martians >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.7> +++"
write "+++ RisultatoComando1 +++"
/sbin/sysctl net.ipv4.icmp_echo_ignore_broadcasts >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.8> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.icmp_ignore_bogus_error_responses >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<3.1.9> +++"
write "+++ RisultatoComando1 +++"
 /sbin/sysctl net.ipv4.conf.all.rp_filter >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
/sbin/sysctl net.ipv4.conf.default.rp_filter >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<3.2.1> +++"
write "+++ RisultatoComando1 +++"
 ifconfig -a >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<3.3.1> +++"
write "+++ RisultatoComando1 +++"
modprobe -c | grep ipv6 >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<3> +++"
write "+++ idCategoriaVerifica:<4> +++"
write "+++ idVerifica:<3.4.1> +++"
write "+++ RisultatoComando1 +++"
rpm -q tcp_wrappers >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
rpm -q tcp_wrappers-libs >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<4.1.1> +++"
write "+++ RisultatoComando1 +++"
for user in smmsp pcap desktop rpc ftp vcsa gopher games uucp news lp; do
 cmd=`grep -w ${user} /etc/passwd`
 if  [ "`echo $?`" -eq "0" ];then
 stat=`passwd --status ${user} | awk '{ print $2 }'`
  if [ "${stat}" != "LK" ]; then
   echo "Account ${user} is not locked." >> $REPORT_FILE
 fi
fi
done
write "+++ RisultatoComando2 +++"

useradd -D | grep INACTIVE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<4.1.2> +++"
write "+++ RisultatoComando1 +++"
 grep lp /etc/group >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep news /etc/group >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep uucp /etc/group >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
grep games /etc/group >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<4.1.3> +++"
write "+++ RisultatoComando1 +++"
if [ "`echo $PATH | grep :: `" != "" ]; then
 echo "Empty Directory in PATH (::)" >> $REPORT_FILE
fi
if [ "`echo $PATH | grep :$`" != "" ]; then
 echo "Trailing : in PATH" >> $REPORT_FILE
fi
p=`echo $PATH | sed -e 's/::/:/' -e 's/:$//' -e 's/:/ /g'`
set -- $p
while [ "$1" != "" ]; do
 if [ "$1" = "." ]; then
  echo "PATH contains ." >> $REPORT_FILE
  shift
  continue
 fi
 if [ -d $1 ]; then
  dirperm=`ls -ldH $1 | cut -f1 -d" "`
  if [ `echo $dirperm | cut -c6 ` != "-" ]; then
   echo "Group Write permission set on directory $1" >> $REPORT_FILE
  fi
  if [ `echo $dirperm | cut -c9 ` != "-" ]; then
   echo "Other Write permission set on directory $1" >> $REPORT_FILE
  fi
  dirown=`ls -ldH $1 | awk '{print $3}'`
  if [ "$dirown" != "root" ] ; then
   echo "$1 is not owned by root" >> $REPORT_FILE
  fi
 else
  echo "$1 is not a directory" >> $REPORT_FILE
 fi
 shift
done
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.1> +++"
write "+++ RisultatoComando1 +++"
grep PASS_MAX_DAYS /etc/login.defs >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
chage --list root | grep "Maximum number" >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep PASS_MIN_DAYS /etc/login.defs >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
chage --list root | grep "Minimum number" >> $REPORT_FILE
write "+++ RisultatoComando5 +++"
grep PASS_WARN_AGE /etc/login.defs >> $REPORT_FILE
write "+++ RisultatoComando6 +++"
chage --list root | grep "Number of days of warning" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.10> +++"
write "+++ RisultatoComando1 +++"
 /bin/grep '^+:' /etc/passwd >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
/bin/grep '^+:' /etc/shadow >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
/bin/grep '^+:' /etc/group >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.2> +++"
write "+++ RisultatoComando1 +++"
grep pam_pwquality.so /etc/pam.d/password-auth >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep pam_pwquality.so /etc/pam.d/system-auth >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep ^minlen /etc/security/pwquality.conf >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
grep ^dcredit /etc/security/pwquality.conf >> $REPORT_FILE
write "+++ RisultatoComando5 +++"
grep ^lcredit /etc/security/pwquality.conf >> $REPORT_FILE
write "+++ RisultatoComando6 +++"
grep ^ocredit /etc/security/pwquality.conf >> $REPORT_FILE
write "+++ RisultatoComando7 +++"
grep ^ucredit /etc/security/pwquality.conf >> $REPORT_FILE
write "+++ RisultatoComando8 +++"
grep remember /etc/pam.d/password-auth >> $REPORT_FILE
write "+++ RisultatoComando9 +++"
grep remember /etc/pam.d/system-auth >> $REPORT_FILE
write "+++ RisultatoComando10 +++"
grep UsePAM /etc/ssh/sshd_config >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.3> +++"
write "+++ RisultatoComando1 +++"
/bin/cat /etc/shadow | /bin/awk -F: '($2 == "" ) { print $1 " does not have a password "}' >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.4> +++"
write "+++ RisultatoComando1 +++"
authconfig --test | grep hashing | grep sha512 >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep password /etc/pam.d/password-auth|grep sufficient|grep "pam_unix.so sha512" >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep password /etc/pam.d/system-auth|grep sufficient|grep "pam_unix.so sha512" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.5> +++"
write "+++ RisultatoComando1 +++"
/bin/cat /etc/passwd | /bin/awk -F: '($3 == 0) { print $1 }' >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.6> +++"
write "+++ RisultatoComando1 +++"
cat /etc/passwd | cut -f3 -d":" | sort -n | uniq -c | while read x ; do
 [ -z "${x}" ] && break
 set - $x
 if [ $1 -gt 1 ]; then
  users=`awk -F: '($3 == n) { print $1 }' n=$2 /etc/passwd | xargs`
  echo "Duplicate UID ($2): ${users}" >> $REPORT_FILE
 fi
done
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.7> +++"
write "+++ RisultatoComando1 +++"
cat /etc/group | cut -f3 -d":" | sort -n | uniq -c | while read x ; do
 [ -z "${x}" ] && break
 set - $x
 if [ $1 -gt 1 ]; then
  groups=`awk -F: '($3 == n) { print $1 }' n=$2 /etc/group | xargs`
  echo "Duplicate GID ($2): ${groups}" >> $REPORT_FILE
 fi
done
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.8> +++"
write "+++ RisultatoComando1 +++"
tUser="dhcpserv smmsp pcap desktop rpc ftp gdm vcsa gopher games openldap uucp news lp webservd daemon bin sys adm lp uucp nuucp smmsp listen gdm postgres svctag nobody noaccess nobody4 unknown"
getent passwd | awk -F: '{ print $1" "$6 }' | while read user dir
do
found=0
cmd=`echo "$tUser" | grep -w "$user"`
if [ "$?" -eq "0" ]
then
found=1
echo "$user found in tUser" >> $REPORT_FILE
fi
if [ $found -eq 0 ]
then
if [ -d $dir ]
then
owner=`ls -ld $dir/. | awk '{ print $3 }'`
if [ "$owner" != "$user" ]
then
echo "Home directory for $user is owned by $owner" >> $REPORT_FILE
fi
else
echo "$user has no home directory" >> $REPORT_FILE
fi
fi
done
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<4.2.9> +++"
write "+++ RisultatoComando1 +++"
 getent passwd | cut -f1 -d":" | sort | uniq -d >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
getent group | cut -f1 -d":" | sort | uniq -d >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<4.3.1> +++"
write "+++ RisultatoComando1 +++"
 # "ANALISI MANUALE" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<4> +++"
write "+++ idVerifica:<4.4.1> +++"
write "+++ RisultatoComando1 +++"
 grep "root" /etc/cron.allow >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
ls -la /etc/cron.deny >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
cat /etc/cron.d/at.allow >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
ls -la /etc/at.deny >> $REPORT_FILE
write "+++ RisultatoComando5 +++"
ls -l /etc/cron.allow | awk '{print $1 " " $3 " " $4 " " $9}' >> $REPORT_FILE
write "+++ RisultatoComando6 +++"
ls -l /etc/at.allow | awk '{print $1 " " $3 " " $4 " " $9}' >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<5> +++"
write "+++ idVerifica:<4.5.1> +++"
write "+++ RisultatoComando1 +++"
grep "^PermitRootLogin" /etc/ssh/sshd_config >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<5> +++"
write "+++ idVerifica:<4.5.2> +++"
write "+++ RisultatoComando1 +++"
# "ANALISI MANUALE" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<5> +++"
write "+++ idVerifica:<4.5.3> +++"
write "+++ RisultatoComando1 +++"
 grep "deny=3 unlock_time=1800" /etc/pam.d/password-auth >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep "deny=3 unlock_time=1800" /etc/pam.d/system-auth >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<5> +++"
write "+++ idVerifica:<4.5.4> +++"
write "+++ RisultatoComando1 +++"
 grep pam_wheel.so /etc/pam.d/su|grep required >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep wheel /etc/group >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<6> +++"
write "+++ idVerifica:<4.5.2> +++"
write "+++ RisultatoComando1 +++"
stat /etc/ssh/sshd_config >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<6> +++"
write "+++ idVerifica:<4.6.1> +++"
write "+++ RisultatoComando1 +++"
 grep "^LogLevel" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep "^X11Forwarding" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep "^MaxAuthTries" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
grep "^IgnoreRhosts" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando5 +++"
grep "^HostbasedAuthentication[[:blank:]]" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando6 +++"
grep "^PermitEmptyPasswords[[:blank:]]" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando7 +++"
grep "^PermitUserEnvironment" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando8 +++"
grep "^Ciphers" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando9 +++"
grep "^ClientAliveInterval" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando10 +++"
grep "^ClientAliveCountMax" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando11 +++"
grep "^AllowUsers" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando12 +++"
grep "^AllowGroups" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando13 +++"
grep "^DenyUsers" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando14 +++"
grep "^DenyGroups" /etc/ssh/sshd_config >> $REPORT_FILE
write "+++ RisultatoComando15 +++"
grep "^Protocol" /etc/ssh/sshd_config >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<4> +++"
write "+++ idCategoriaVerifica:<7> +++"
write "+++ idVerifica:<4.7.1> +++"
write "+++ RisultatoComando1 +++"
 grep "^Banner[[:blank:]]" /etc/ssh/sshd_config >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<5> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<5.1.1> +++"
write "+++ RisultatoComando1 +++"
services="avahi-daemon cups dhcpd slapd nfs rpcbind named vsftpd httpd dovecot smb squid snmpd ypserv rsh.socket rlogin.socket rexec.socket ntalk telnet.socket tftp.socket rsyncd finger-server"
for serv in $services
do
check=`systemctl is-enabled $serv`
echo $check
if [ "$check" = "enabled" ]
then
echo "Service $serv is enabled" >> $REPORT_FILE
fi
done
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<5> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<5.1.2> +++"
write "+++ RisultatoComando1 +++"
clients="ypbind rsh talk telnet penldap-clients finger"
for client in $clients
do
cmd=`rpm -q $client`
if [ "$?" -eq "0" ]
then
echo "Found $client installed" >> $REPORT_FILE
fi
done  
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<5> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<5.1.3> +++"
write "+++ RisultatoComando1 +++"
rpm -qa xorg-x11* >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<5> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<5.1.4> +++"
write "+++ RisultatoComando1 +++"
systemctl is-enabled xinetd >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
services="chargen-dgram chargen-stream daytime-dgram daytime-stream discard-dgram discard-stream echo-dgram echo-stream time-dgram time-stream tftp"
for serv in $services
do
cmd=`chkconfig --list | grep -w $serv | awk '{print $2}'`
if [ "$cmd" = "on" ]
then
echo "xinetd service is on ($serv)" >> $REPORT_FILE
fi
done
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<5> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<5.1.5> +++"
write "+++ RisultatoComando1 +++"
cmd=`netstat -an | grep LIST | grep ":25[[:space:]]"|awk '{print $4}'`
for out in $cmd
do
if [ "$out" = "127.0.0.1:25" ] || [ "$out" = "::1:25" ]
then
echo "$out is OK" >> $REPORT_FILE
else
echo "$out not correctly configured" >> $REPORT_FILE
fi
done
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<5> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<5.2.1> +++"
write "+++ RisultatoComando1 +++"
 rpm -q vsftpd >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
systemctl is-enabled vsftpd >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
netstat -an | grep LIST | grep ":21[[:space:]]" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<5> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<5.2.2> +++"
write "+++ RisultatoComando1 +++"
grep "^restrict" /etc/ntp.conf >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep "^server" /etc/ntp.conf >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep "^OPTIONS" /etc/sysconfig/ntpd >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
grep "^ExecStart" /usr/lib/systemd/system/ntpd.service >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<5> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<5.2.3> +++"
write "+++ RisultatoComando1 +++"
 grep "^*.*[^I][^I]*@" /etc/rsyslog.conf >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep '$ModLoad imtcp.so' /etc/rsyslog.conf >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep '$InputTCPServerRun' /etc/rsyslog.conf >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<6.1.1> +++"
write "+++ RisultatoComando1 +++"
 rpm -q rsyslog >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
systemctl is-enabled syslog >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
systemctl is-enabled rsyslog >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<6.1.2> +++"
write "+++ RisultatoComando1 +++"
stat /etc/rsyslog.conf >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.1> +++"
write "+++ RisultatoComando1 +++"
 systemctl is-enabled auditd >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep max_log_file /etc/audit/auditd.conf >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
grep max_log_file_action /etc/audit/auditd.conf >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.10> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "x86_64" ]
then
grep -E 'arch=b64.*perm_mod' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.11> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "i386" ]
then
grep -E 'arch=b32.*access' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.12> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "x86_64" ]
then
grep -E 'arch=b64.*access' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.13> +++"
write "+++ RisultatoComando1 +++"
 grep scope /etc/audit/audit.rules >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.14> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "i386" ]
then
grep -E 'arch=b32.*mounts' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.15> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "x86_64" ]
then
grep -E 'arch=b64.*mounts' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.16> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "i386" ]
then
grep -E 'arch=b32.*delete' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.17> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "x86_64" ]
then
grep -E 'arch=b64.*delete' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.18> +++"
write "+++ RisultatoComando1 +++"
grep actions /etc/audit/audit.rules >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.19> +++"
write "+++ RisultatoComando1 +++"
grep scope /etc/audit/audit.rules >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.2> +++"
write "+++ RisultatoComando1 +++"
grep "^\s*linux" /boot/grub2/grub.cfg >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.20> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "i386" ]
then
grep -E 'arch=b32.*modules' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.21> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "x86_64" ]
then
grep -E 'arch=b64.*modules' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.22> +++"
write "+++ RisultatoComando1 +++"
grep "^\s*[^#]" /etc/audit/audit.rules | tail -1  >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.3> +++"
write "+++ RisultatoComando1 +++"
 grep time_change /etc/audit/audit.rules >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.4> +++"
write "+++ RisultatoComando1 +++"
 grep identity /etc/audit/audit.rules >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.5> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "i386" ]
then
grep -E 'arch=b32.*system-locale' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.6> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "x86_64" ]
then
grep -E 'arch=b64.*system-locale' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.7> +++"
write "+++ RisultatoComando1 +++"
 grep logins /etc/audit/audit.rules >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.8> +++"
write "+++ RisultatoComando1 +++"
 grep session /etc/audit/audit.rules >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<6> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<6.2.9> +++"
write "+++ RisultatoComando1 +++"
if [ "`uname -i`" = "i386" ]
then
grep -E 'arch=b32.*perm_mod' /etc/audit/audit.rules >> $REPORT_FILE
else
echo "Verifica Non Applicabile" >> $REPORT_FILE
fi
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<7> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<7.1.1> +++"
write "+++ RisultatoComando1 +++"
 # "ANALISI MANUALE" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<8> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<8.1.1> +++"
write "+++ RisultatoComando1 +++"
 # "ANALISI MANUALE" >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<9> +++"
write "+++ idCategoriaVerifica:<1> +++"
write "+++ idVerifica:<9.1.1> +++"
write "+++ RisultatoComando1 +++"
 rpm -q aide >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
crontab -u root -l | grep aide >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<9> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<9.2.1> +++"
write "+++ RisultatoComando1 +++"
grep "^\s*linux" /boot/grub2/grub.cfg| grep -E "selinux=0|enforcing=0" >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
grep SELINUX=enforcing /etc/selinux/config >> $REPORT_FILE
write "+++ RisultatoComando3 +++"
sestatus >> $REPORT_FILE
write "+++ RisultatoComando4 +++"
sestatus >> $REPORT_FILE
write "+++ RisultatoComando5 +++"
sestatus >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<9> +++"
write "+++ idCategoriaVerifica:<2> +++"
write "+++ idVerifica:<9.2.2> +++"
write "+++ RisultatoComando1 +++"
rpm -q setroubleshoot >> $REPORT_FILE
write "+++ RisultatoComando2 +++"
rpm -q mcstrans >> $REPORT_FILE
write "******************************************************************************************************>"
write "<******************************************************************************************************"
write "+++ idAmbitoVerifica:<9> +++"
write "+++ idCategoriaVerifica:<3> +++"
write "+++ idVerifica:<9.3.1> +++"
write "+++ RisultatoComando1 +++"
# "ANALISI MANUALE" >> $REPORT_FILE
write "******************************************************************************************************>"
write "***************************************************************"


write "\n***************************************************************"    
write "SICUREZZA TOMCAT"
write "***************************************************************"    

#write "+++ Processo Tomcat attivo +++\n"
#ps -ef | grep "org.apache.catalina.startup.Bootstrap" | grep -v "grep" >> $REPORT_FILE
#COMMAND="`ps -ef | grep "org.apache.catalina.startup.Bootstrap" | grep -v "grep"`"
#
#if [ "$COMMAND" != "" ]
#then
## Crea la directory "audit/tomcat.$(hostname)/test" dove memorizzare i risultati dell'esecuzione
## nel caso non sia stata gi� creata
#	if  [ ! -e "audit/tomcat.$(hostname)/test" ]
#	then
#		write "+++ Crea la directory audit/tomcat.$(hostname) dove memorizzare i risultati dell'esecuzione +++\n"
#		mkdir -p audit/tomcat.$(hostname)/test
#	fi
#
#
#	write "+++ Utente Tomcat attivo +++\n"
#	echo "$COMMAND" | awk '{print $1}' >> $REPORT_FILE
#	TOMCAT_USER="`echo "$COMMAND" | awk '{print $1}'`"
#	
#	write "+++ UID utente Tomcat attivo +++\n"
#	cat /etc/passwd | grep -E "^$TOMCAT_USER:" | cut -d ":" -f3 >> $REPORT_FILE
#	TOMCAT_UID="`cat /etc/passwd | grep -E "^$TOMCAT_USER:" | cut -d ":" -f3`"
#	MIN_UID="`cat /etc/login.defs | grep -E "^UID_MIN" | awk '{print $2}'`"
#	
#	write "\n+++ ***** ANALISI: -> Sicurezza utente Tomcat ***** +++\n"
#	
#	if [ "$TOMCAT_USER" == "root" ]
#	then
#		write "\n+++ Attenzione: -> l'utente Tomcat � 'root' +++\n"
#	else
#		if [ $TOMCAT_UID -gt $MIN_UID ]
#	  then
#	  	write "\n+++ L'utente Tomcat = $TOMCAT_USER($TOMCAT_UID) � [OK] +++\n"
#	  else
#	  	write "\n+++ Attenzione: -> l'utente Tomcat = $TOMCAT_USER($TOMCAT_UID) $MIN_UID dovrebbe essere meno di $TOMCAT_UID +++\n"
#	  fi
#	fi
#	
#	#TROVA LA HOME DIRECTORY DI TOMCAT 
#	write "\n+++ ***** ANALISI: -> Verifica presenza di un processo Tomcat attivo sul server in analisi: - comando: ps -ef | grep 'org.apache.catalina.startup.Bootstrap' | grep -v 'grep' | wc -l` ***** +++\n"
#	if [ -z "$COMMAND" ]
#	then
#		write "\n+++ Attenzione: -> Non risulta essere attivo un processo Apache Tomcat +++\n"
#	else
#		write "\n+++ *****ANALISI: -> Verifica presenza di pi� processi Tomcat attivi sul server in analisi: - comando: ps -ef | grep 'org.apache.catalina.startup.Bootstrap' | grep -v 'grep' | wc -l` +++\n"
#		if [ "`ps -ef | grep 'org.apache.catalina.startup.Bootstrap' | grep -v 'grep' | wc -l`" -gt 1 ]
#	  then
#			write "\n+++ Errore Critico: -> -> E' presente pi� di un processo attivo sul server in analisi!!! +++\n"
#	  else
#	  	write "\n+++ Info: -> Informazioni relative a Tomcat - esecuzione script: $TOMCAT_HOME/bin/version.sh +++\n"
#			if [[  "`echo $COMMAND`" =~ "-Djava.security.manager" ]]
#	    then
#	    	
#	    	write "\n+++ *****ANALISI: -> Verifica presenza di Tomcat Security Manager per lo startup del processo sul server in analisi: - presenza in PS di: -Djava.security.manager ***** +++\n"
#	    	if [[ "`echo $COMMAND`" =~ "-Dcatalina.home=(.*) -" ]]
#	    	then
#	      	if [ -d "`echo "${BASH_REMATCH[1]}"`" ]
#	        then
#						TOMCAT_HOME="${BASH_REMATCH[1]}"
#						VERSION_COMMAND="`"$TOMCAT_HOME"/bin/version.sh`"
#						SERVER_VERSION="`echo "$VERSION_COMMAND" | grep -E "^Server version:"`"
#						JVM_VERSION="`echo "$VERSION_COMMAND" | grep -E '^JVM Version:'`"
#						write "\n+++ Info: -> Versione Server e JVM: $SERVER_VERSION - $JVM_VERSION  +++\n"					
#	       	else
#	       		write "\n+++ Errore Critico: -> il comando ps riporta che la Home Directory di Tomcat � ${BASH_REMATCH[1]}, ma non risulta esistere in realt� !!!  +++\n"					
#	        fi
#	    	else
#	    		write "\n+++ Errore Critico: -> Non � stato possibile trovare la Home Directory Catalina.home nell'output del comando ps!!!  +++\n"					
#	    	fi
#			else
#				write "\n+++ Attenzione: -> Non � stata utilizzato il Tomcat Security Manager per lo startup del processo.  +++\n"					
#	      if [[ "`echo $COMMAND`" =~ "-Dcatalina.home=(.*) -" ]]
#	    	then
#	      	if [ -d "`echo "${BASH_REMATCH[1]}"`" ]
#	        then
#						TOMCAT_HOME="${BASH_REMATCH[1]}"
#						VERSION_COMMAND="`"$TOMCAT_HOME"/bin/version.sh`"
#						SERVER_VERSION="`echo "$VERSION_COMMAND" | grep -E "^Server version:"`"
#						JVM_VERSION="`echo "$VERSION_COMMAND" | grep -E '^JVM Version:'`"
#	 
#						write "\n+++ Info: -> Versione Server e JVM: $SERVER_VERSION - $JVM_VERSION  +++\n"				
#	        else
#	          write "\n+++ Errore Critico: -> il comando ps riporta che la Home Directory di Tomcat � ${BASH_REMATCH[1]}, ma non risulta esistere in realt� !!!  +++\n"					
#	        fi
#	    	else
#	        write "\n+++ Errore Critico: -> Non � stato possibile trovare la Home Directory Catalina.home nell'output del comando ps!!!  +++\n"					
#	    	fi
#			fi
#		fi    
#	
#	
#	#Directory_Listing
#	write "\n+++ *****ANALISI: -> Verifica presenza di Directory_Listing sul server in analisi ***** +++\n"
#	
#	Tomcat_Extract_Conf "web.xml" "<!--" "-->" "audit/tomcat.$(hostname)/test/web.xml"
#	    SHOW_PARAM_LISTING="no"
#	    write "\n+++ Info: -> Estrazione file di configurazione web.xml ed analisi per test Directory_Listing +++\n"					
#	    WEB_XML="audit/tomcat.$(hostname)/test/web.xml"
#	
#	    cat $WEB_XML | while read line
#	    do
#	        if [[ "$line" =~ "<init-param>" ]]
#	        then
#	            SHOW_PARAM_LISTING="yes"
#	        fi
#	        
#	        if [ $SHOW_PARAM_LISTING == "yes" ]
#	        then
#	            if [[ "$line" =~ "<param-name>(.*)</param-name>" ]]
#	            then
#	            		write "\n+++ Info: -> param-name=${BASH_REMATCH[1]}  +++\n"					
#	                #echo "param-name=${BASH_REMATCH[1]}"
#	            elif [[ "$line" =~ "<param-value>(.*)</param-value>" ]]
#	            then
#	            		write "\n+++ Info: -> param-value=${BASH_REMATCH[1]}  +++\n"					
#	                #echo "param-value=${BASH_REMATCH[1]}"
#	            fi
#	        fi
#	        
#	        if [[ "$line" =~ "</init-param>" ]]
#	        then
#	            SHOW_PARAM_LISTING="no"
#	        fi
#	    done
#	    
#	# VERSIONE TOMCAT
#	write "\n+++ *****ANALISI: -> Estrazione della versione di Tomcat in uso ***** +++\n"
#	if [ ! -d "$TOMCAT_HOME/lib" ]
#	    then
#	    		write "\n+++ Attenzione: -> Non risulta essere presente la directory $TOMCAT_HOME/lib +++\n"
#	    else
#	        cd $TOMCAT_HOME/lib
#	        
#	        if [ -d "audit/tomcat.$(hostname)/test/catalinajar_extract_to" ]
#	        then
#	            echo "rm -rf audit/tomcat.$(hostname)/test/catalinajar_extract_to"
#	        else
#	            mkdir audit/tomcat.$(hostname)/test/catalinajar_extract_to
#	            
#	            if [ ! -f catalina.jar ]
#	            then
#	                write "\n+++ Attenzione: -> La classe catalina.jar non risulta essere presente +++\n"
#	            else
#	            		write "\n+++ Info: -> Estrazione classe catalina.jar - Comando 'unzip -q catalina.jar -d audit/tomcat.$(hostname)/test/catalinajar_extract_to'  +++\n"					
#	                unzip -q catalina.jar -d audit/tomcat.$(hostname)/test/catalinajar_extract_to
#	                
#	                write "\n+++ Info: -> `cat audit/tomcat.$(hostname)/test/catalinajar_extract_to/org/apache/catalina/util/ServerInfo.properties | grep -E -v '^#' | grep -E -v '^$' | grep -E '^server.info='`  +++\n"					
#	                rm -rf audit/tomcat.$(hostname)/test/catalinajar_extract_to
#	            fi    
#	        fi
#	    fi
#	    
#	        Tomcat_Extract_Conf "server.xml" "<!--" "-->" "audit/tomcat.$(hostname)/test/server.xml"
#	        CONNECTOR="no"
#	        SERVER_XML="audit/tomcat.$(hostname)/test/server.xml"
#	        cat $SERVER_XML | grep -E -v "^$" | while read line
#	        do
#	            if [[ "$line" =~ "<Connector .*" ]]
#	            then
#	                CONNECTOR="yes"    
#	            fi
#	            
#	            if [ "$CONNECTOR" == "yes" ]
#	            then
#	                if [[ "$line" =~ "server=\"(.*)\"" ]]
#	                then
#	                		write "\n+++ Info: -> server= ${BASH_REMATCH[1]}  +++\n"					
#	                    #echo " -> server= ${BASH_REMATCH[1]}"
#	                fi  
#	            fi
#	            
#	            if [[  "$line" =~ ".*/>" ]]
#	            then
#	                CONNECTOR="no"    
#	            fi            
#	        done
#	
#	
#	#Shutdown_Tomcat_Port_Password
#	write "\n+++ *****ANALISI: -> Shutdown_Tomcat_Port_Password ***** +++\n"
#	SERVER_XML="audit/tomcat.$(hostname)/test/server.xml"
#	    
#	    cat $SERVER_XML | grep -E -v "^$" | while read line
#	    do
#	        if [[ "$line" =~ "<Server .* shutdown=\"(.*)\"" ]]
#	        then
#	            if [ -n "${BASH_REMATCH[1]}" ]
#	            then
#	            		write "\n+++ Attenzione: -> � presente l'opzione di shutdown remota= ${BASH_REMATCH[1]} +++\n"
#	                #echo "Attenzione -> � presente l'opzione di shutdown remota= ${BASH_REMATCH[1]}"
#	            else
#	            		write "\n+++ Info: -> L'opzione di shutdown � vuota  +++\n"					
#	                 #echo "L'opzione di shutdown � vuota!!!"
#	            fi
#	        fi
#	    done
#	#
#	
#	#Restrict_Admin_Manager
#	write "\n+++ *****ANALISI: -> Restrict_Admin_Manager ***** +++\n"
#	    Tomcat_Restrict_Admin_Manager_Tmp "admin.xml"    
#	    Tomcat_Restrict_Admin_Manager_Tmp "manager.xml"
#	
#	#
#	
#	#Logging_Option
#	write "\n+++ *****ANALISI: -> Opzioni di logging Tomcat ***** +++\n"
#	    if [ ! -d $TOMCAT_HOME/logs ]
#	    then
#	    		write "\n+++ Attenzione: -> La directory dei Logs non � presente +++\n"
#	        #echo "Attenzione: -> La directory dei Logs non � presente!!!"
#	    else
#	        ls -1 $TOMCAT_HOME/logs | while read line
#	        do
#	            if  [[ "$line" =~ "^catalina\.[0-9]{4}\-[0-9]{,2}\-[0-9]{,2}\.log$" ]]
#	            then
#	            		write "\n+++ Info: -> OK: -> ${BASH_REMATCH} +++\n"					
#	                #echo "OK: -> ${BASH_REMATCH}"
#	            elif [[ "$line" =~ "^catalina\.out$" ]]
#	            then
#	            		write "\n+++ Info: -> OK: -> ${BASH_REMATCH} +++\n"					
#	                #echo "OK: -> ${BASH_REMATCH}"
#	            fi
#	        done
#	    fi
#	    
#	    
#	    ls -1 $TOMCAT_HOME/webapps | while read line2
#	    do
#	        if [ -f $TOMCAT_HOME/webapps/$line2/WEB-INF/classes/logging.properties -o -f $TOMCAT_HOME/webapps/$line2/WEB-INF/classes/log4j.properties ]
#	        then
#	        		write "\n+++ Info: -> $line2=le ozioni di logging sono [OK]  +++\n"					
#	            #echo "OK: -> $line2=le ozioni di logging sono [OK]"
#	        fi
#	    done
#	#
#	
#	#Ssl
#	write "\n+++ *****ANALISI: -> SSL Tomcat ***** +++\n"
#	CONNECTOR="no"
#	    SSLPresente="no"
#	    cat $SERVER_XML | while read line 
#	    do
#	        if [[  "$line" =~ "<Connector .*" ]]
#	        then
#	                CONNECTOR="yes" 
#	        fi
#	        
#	        if [ "$CONNECTOR" == "yes" ]
#	        then
#	            if [[ "$line" =~ "sslProtocol=(\"[a-zA-Z]*\") .* />" ]]
#	            then
#	            		write "\n+++ Info: -> Ssl=${BASH_REMATCH[1]}  +++\n"					
#	                #echo "SSL -> Ssl=${BASH_REMATCH[1]}"
#	                SSLPresente="si"
#	            fi
#	        fi
#	        
#	        if [[  "$line" =~ ".* />" ]]
#	        then
#	                CONNECTOR="no" 
#	        fi
#	    done
#	    if [ $SSLPresente="no" ] 
#	    then
#	    	write "\n+++ Attenzione: -> Non viene utilizzato SSL +++\n"
#	    	#echo "Attenzione: -> Non viene utilizzato SSL"
#	    fi
#	#
#	
#	# Dir_File_Access
#	write "\n+++ *****ANALISI: -> permessi sulle directory Tomcat - esecuzione comando ls -lR  $TOMCAT_HOME 2>/dev/null | grep -v "total" ***** +++\n"
#	    ls -lR  $TOMCAT_HOME 2>/dev/null | grep -v "total" >> $REPORT_FILE
#	#
#	
#	
#	if  [ ! -e audit/tomcat.$(hostname)/backuphomedir ]
#	then
#		mkdir -p audit/tomcat.$(hostname)/backuphomedir
#	fi
#			
#	write "\n+++ Copia File HomeDir TOMCAT +++\n"
#	cp -r $TOMCAT_HOME audit/tomcat.$(hostname)/backuphomedir
#	
#	#write "\n+++ COPIA FILE TOMCAT +++\n"
#	#cp -r $TOMCAT_HOME/webapps audit/tomcat.$(hostname)/webapps
#	
#	#write "\n+++ COPIA FILE TOMCAT +++\n"
#	#cp -r $TOMCAT_HOME/conf audit/tomcat.$(hostname)/bin
#	
#	#write "\n+++ COPIA FILE TOMCAT +++\n"
#	#cp -r $TOMCAT_HOME/conf audit/tomcat.$(hostname)/lib
#	
#	#write "\n+++ COPIA FILE TOMCAT +++\n"
#	#cp -r $TOMCAT_HOME/conf audit/tomcat.$(hostname)/work
#else
#	write "\n+++Non � stato possibile rilevare il processo Tomcat attivo +++\n"
#fi

write "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"

write "\n***************************************************************"    
write "SICUREZZA JBOSS"
write "***************************************************************"    

if [ "$JBOSS_HOME" != "" ]
then
	# Crea la directory "audit/jboss.$(hostname)" dove memorizzare i file di configurazione di jboss
	# nel caso non sia stata gi� creata
	if  [ ! -e "audit/jboss.$(hostname)" ]
	then
		write "+++ Crea la directory audit/jboss.$(hostname) dove memorizzare i file di configurazione di jboss +++\n"
		mkdir audit/jboss.$(hostname)/
	fi
	write "\n+++ACQUISISCO CARTELLA CONTENENTE CONFIGURAZIONI JBOSS +++\n"
	cp -r $JBOSS_HOME/client audit/jboss.$(hostname)/
	cp -r $JBOSS_HOME/common audit/jboss.$(hostname)/
	cp -r $JBOSS_HOME/docs audit/jboss.$(hostname)/
	cp -r $JBOSS_HOME/lib audit/jboss.$(hostname)/
	cp -r $JBOSS_HOME/server audit/jboss.$(hostname)/
else
	write "\n+++Non è stato possibile rilevare la path relativa all'installazione di jboss +++\n"
fi
write "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"

write "\n\nFine esecuzione script: $(date +%H%M%S) \n\n"

exit 0
######################################################################

# End of script
) 2>&1 |tee $REPORT_FILE_WITH_ERROR
