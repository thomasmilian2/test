## Ansible + Spacewalk RHEL 6/7

This is an ansible playbook for automatically register an host to Spacewalk and choose automatically the RHEL6 or 7 repository.

### Usage (check run)
    ansible-playbook -v -i hosts -C playbook.yml

### Usage (apply)
    ansible-playbook -v -i hosts playbook.yml
