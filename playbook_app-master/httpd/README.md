## Ansible Playbook per installazione e configurazione Tomcat 8.5.31

Questo playbook contiene : Tomcat 8.5.31 + JDK 1.8_172 + Hardening Tomcat + Librerie Custom + Systemd service 

### Usage (check run)
    ansible-playbook -v -i hosts -C playbook.yml

### Usage (apply)
    ansible-playbook -v -i hosts playbook.yml
